const express = require('express');
const bodyParser = require('body-parser')
const bcrypt = require('bcrypt-nodejs');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

const db = {
    users: [
    {
        id: '123',
        name: 'John',
        password: 'cookies',
        email: 'john@rr.cc',
        entries: 0,
        joined: new Date()
    },
    {
        id: '144',
        name: 'Mary',
        password: 'bananas',
        email: 'mary@rr.cc',
        entries: 0,
        joined: new Date()
    }
    ],
    login: [
        {
            id: '987',
            hash: '',
        }
    ]
}

app.get('/', (req, res) => {
    res.send(db.users);
})

app.post('/signin', (req, res) => {
    // Load hash from your password DB.
    // bcrypt.compare("apples", hash, function(err, res) {
    //     // res == true
    // });
    console.log(req.body.email, req.body.password);
    if(req.body.email === db.users[0].email &&
       req.body.password === db.users[0].password) {
        res.json(db.users[0]);
    } else {
        res.status(400).json('error logging in');
    }
})

app.post('/register', (req, res) => {
    const {email, name, password} = req.body;
    bcrypt.hash(password, null, null, (err, hash) => {
        console.log(hash);
    });
    db.users.push(
        {
            id: '145',
            name: name,
            email: email,
            entries: 0,
            joined: new Date()
        }
    );
    res.json(db.users[db.users.length-1]);
})

app.get('/profile/:id', (req, res) => {
    const { id } = req.params;
    const userData = db.users.filter(user => user.id === id);

    userData.length !== 0 ? 
        res.json(userData) :
        res.status(404).json('User not found');
})

app.put('/image', (req, res) => {
    const { id } = req.body;
    let found = false;
    db.users.forEach(user => {
        if(user.id === id) {
            found = true;
            user.entries++;
            return res.json(user.entries);
        }
        if (!found) {
            res.status(404).json('User not found');
        }
    });
})

app.listen(3001, () => {
    console.log("App is running on 3001");
})
